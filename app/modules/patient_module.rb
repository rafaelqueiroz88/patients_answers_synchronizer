#!/usr/bin/ruby

# This class should control patients without area and/or quiz
class Patient

  require 'progress_bar'

  # Constructor method
  def initialize mysql, customer
    @mysql = mysql
    @customer = customer
  end

  # Define the collection
  # This method is called from ReturnUpdater.start_process
  def define_collection collection
    @collection = collection
  end

  def remove_patients

    # Set start parameters
    total = @collection.count
    deleted = 0
    errors = 0

    puts "Iniciando a limpeza\n#{total} registros duplicados encontrados\nIniciando correção"

    @bar = ProgressBar.new total

    # For each register, do create interviewed array and then...
    @collection.each do |interviewed|

      customer = interviewed['customer']
      search = interviewed['SEARCH_CMD']

      if @mysql.delete_duplicated_patient search, customer
        deleted = deleted + 1
      else
        errors = errors + 1
      end

      @bar.increment!
    end

    define_result [total, deleted, errors]
  end

  # Update area and quiz
  # This method is called from patients.start_process
  def update_patients starting, ending

    # Set start parameters
    total = @collection.count
    updated = 0
    errors = 0

    puts "Iniciando a atualização\n#{total} registros encontrados sem Área e/ou Questionário\nIniciando correção"

    @bar = ProgressBar.new total

    # For each register, do create interviewed array and then...
    @collection.each do |interviewed|

      customer = interviewed['customer']
      patient = interviewed['patient']
      a_area = interviewed['answer_area']
      a_quiz = interviewed['answer_quiz']

      if @mysql.update_patient_area_and_quiz customer, starting, ending, patient, a_area, a_quiz
        updated = updated + 1
      else
        errors = errors + 1
      end

      @bar.increment!
    end

    define_result [total, updated, errors]
  end

  # This method return stored data from internal processes
  # @return array
  def retrieve_results
    @result
  end


  private

  # This method store an array with the result of the process
  # @require array result
  # @void
  def define_result result
    @result = result
    nil
  end
end