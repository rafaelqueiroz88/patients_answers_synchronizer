#!/usr/bin/ruby

# This class connect the entire system to mysql database 'Maestro'
class MysqlMaestro

  require 'mysql2'

  def initialize env
    @env = env
  end

  def retrieve_duplicated_errors customer, starting, ending
    sql = "SELECT bp.ID_Paciente AS patient, ie.name AS name, ie.email AS email,"
    sql = "#{sql} COUNT(ie.email) AS erros_p_paciente,"
    sql = "#{sql} CONCAT("
    sql = "#{sql} 'SELECT (@id := `id`) as id FROM interviewed_error ie WHERE"
    sql = "#{sql} customer_id = ', ie.customer_id ,' AND email = \"', ie.email, '\" AND createdAt"
    sql = "#{sql} BETWEEN \"', #{starting}, '\" AND \"', #{ending}, '\" LIMIT 1;') AS SEARCH_CMD,"
    sql = "#{sql} 'DELETE FROM interviewed_error "
    sql = "#{sql} WHERE customer_id = ', ie.customer_id ,' AND id > @id AND email = \"', ie.email, '\""
    sql = "#{sql} AND createdAt BETWEEN \"', #{starting}, '\" AND \"', #{ending}, '\";' ) AS DELETE_CMD"
    sql = "#{sql} FROM interviewed_error ie"
    sql = "#{sql} INNER JOIN base_pacientes bp on ie.email = bp.Email"
    sql = "#{sql} WHERE"

    if customer != 0
      sql = "#{sql} bp.ID_Cliente = '#{customer}' AND"
    end

    sql = "#{sql} DATE(bp.Data_Base) BETWEEN \"#{starting}\" AND \"#{ending}\""
    # sql = "#{sql} AND ie.customer_id = #{customer}"
    sql = "#{sql} AND ie.name != ''"
    sql = "#{sql} AND bp.Nome_Completo_Paciente != ''"
    sql = "#{sql} AND ie.name = bp.Nome_Completo_Paciente"
    sql = "#{sql} AND bp.file_ref = ie.ref"
    sql = "#{sql} GROUP BY bp.ID_Paciente"
    sql = "#{sql} HAVING erros_p_paciente > 1;"

    process = "retrieve_duplicated_errors with params: (customer: #{customer}, startig: #{starting}, ending: #{ending})"
    result = run_query sql, customer, process

    puts result.inspect
    exit 0
  end

  def retrieve_duplicated_patients customer, starting, ending
    sql = "SELECT b.Nome_Cliente AS customer, bp.ID_Paciente AS patient,"
    sql = "#{sql} COUNT(br.ID_Paciente) AS respostas_p_paciente,"
    sql = "#{sql} CONCAT("
    sql = "#{sql} 'SELECT br.* FROM base_resultados br WHERE br.ID_Paciente = ', bp.ID_Paciente, ' LIMIT 1;'"
    sql = "#{sql} ) AS SEARCH_CMD"
    sql = "#{sql} FROM base_resultados br"
    sql = "#{sql} INNER JOIN base b ON br.ID_Cliente = b.ID"
    sql = "#{sql} INNER JOIN base_pacientes bp ON bp.ID_Paciente = br.ID_Paciente"
    sql = "#{sql} WHERE"

    if customer != 0
      sql = "#{sql} bp.ID_Cliente = '#{customer}' AND"
    end

    sql = "#{sql} DATE(br.Data_Resposta) BETWEEN '#{starting}' AND '#{ending}'"
    sql = "#{sql} GROUP BY br.ID_Paciente"
    sql = "#{sql} HAVING respostas_p_paciente > 1;"

    process = "retrieve_duplicated_patients with params: (customer: #{customer}, startig: #{starting}, ending: #{ending})"
    result = run_query sql, customer, process
  end

  def retrieve_unset_patients customer, starting, ending
    sql = "SELECT bp.ID_Cliente AS customer, bp.ID_Paciente as patient,"
    sql = "#{sql} bp.Area_Pesquisa as patient_area, bq.Area answer_area,"
    sql = "#{sql} bp.ID_Questionario patient_quiz,"
    sql = "#{sql} br.ID_Questionario as answer_quiz"
    sql = "#{sql} FROM base_pacientes bp"
    sql = "#{sql} INNER JOIN base_resultados br ON bp.ID_Paciente = br.ID_Paciente"
    sql = "#{sql} INNER JOIN base_questionarios bq ON br.ID_Questionario = bq.ID_Questionario"
    sql = "#{sql} WHERE"

    if customer != 0
      sql = "#{sql} bp.ID_Cliente = '#{customer}' AND"
    end

    sql = "#{sql} DATE(bp.Data_Base) BETWEEN '#{starting}' AND '#{ending}'"
    sql = "#{sql} AND ( (bp.ID_Questionario != br.ID_Questionario OR bp.Area_Pesquisa != bq.Area)"
    sql = "#{sql} OR"
    sql = "#{sql} (bp.ID_Questionario IS NULL OR bp.Area_Pesquisa IS NULL) );"

    process = "retrieve_unset_patients with params: (customer: #{customer}, startig: #{starting}, ending: #{ending})"
    result = run_query sql, customer, process
  end

  def delete_duplicated_patient search, customer
    process = "delete_duplicated_patient with params: (search: #{search})"
    result = run_query search, customer, process
    id = 0
    patient = 0
    result.each do |r|
      id = r['id']
      patient = r['ID_Paciente']
    end

    delete = "DELETE FROM base_resultados WHERE id > #{id} AND ID_Paciente = #{patient};"
    process = "delete_duplicated_patient with params: (delete: #{delete})"
    run_query delete, customer, process
    true
  end

  def update_patient_area_and_quiz customer, starting, ending, patient, a_area, a_quiz
    sql = "UPDATE base_pacientes bp"
    sql = "#{sql} INNER JOIN base_resultados br ON bp.ID_Paciente = br.ID_Paciente"
    sql = "#{sql} INNER JOIN base_questionarios bq ON br.ID_Questionario = bq.ID_Questionario"
    sql = "#{sql} SET"
    sql = "#{sql} bp.Area_Pesquisa = '#{a_area}',"
    sql = "#{sql} bp.ID_Questionario = '#{a_quiz}',"
    sql = "#{sql} bp.Data_Envio_Email = br.Data_Envio,"
    sql = "#{sql} bp.Emails_Enviados = 1,"
    sql = "#{sql} bp.Ordem_Email = 1"
    sql = "#{sql} WHERE"
    sql = "#{sql} bp.ID_Cliente = '#{customer}'"
    sql = "#{sql} AND DATE(bp.Data_Base) BETWEEN '#{starting}' AND '#{ending}'"
    sql = "#{sql} AND ( (bp.ID_Questionario != br.ID_Questionario OR bp.Area_Pesquisa != bq.Area)"
    sql = "#{sql} OR"
    sql = "#{sql} (bp.ID_Questionario IS NULL OR bp.Area_Pesquisa IS NULL) )"
    sql = "#{sql} AND bp.ID_Paciente = '#{patient}';"

    process = "update_patient_area_and_quiz with params: (customer: #{customer}, startig: #{starting}, ending: #{ending}, patient: #{patient})"
    result = run_query sql, customer, process
  end

  def log_error error, customer, process

    customer == 0 ? customer = 'Não dectado ou não informado' : customer = customer

    sql = 'INSERT INTO patients_without_area_quiz'
    sql = "#{sql} (message, customer, process, created_at)"
    sql = "#{sql} VALUES"
    sql = "#{sql} (\"#{error}\", \"#{customer}\", \"#{process}\", now());"
    puts sql
    # run_query sql, customer, process
  end


  private

  def define_db_production_setup
    db = [ENV["MAESTRO_HOST"], ENV["MAESTRO_USER"], ENV["MAESTRO_PASS"], ENV["MAESTRO_DB"]]
    @client = Mysql2::Client.new(
        host: db[0], username: db[1], password: db[2], database: db[3]
    )
  end

  def define_db_hmg_setup
    db = [ENV["MAESTRO_HMG_HOST"], ENV["MAESTRO_HMG_USER"], ENV["MAESTRO_HMG_PASS"], ENV["MAESTRO_HMG_DB"]]
    @client = Mysql2::Client.new(
        host: db[0], username: db[1], password: db[2], database: db[3]
    )
  end

  def unset_connection
    @client.close
  end

  def run_query sql, customer, process
    @env == 'prod' ? define_db_production_setup : define_db_hmg_setup
    begin
      result = @client.query sql
      unset_connection
    rescue StandardError => e
      log_error e, customer, process
    end
    result
  end
end