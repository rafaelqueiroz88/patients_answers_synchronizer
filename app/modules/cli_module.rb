#!/usr/bin/ruby
class CLI

  require 'date'

  def initialize
    validate_args
  end

  def retrieve_period_start
    @s_period
  end

  def retrieve_period_end
    @e_period
  end

  def retrieve_customer
    @customer
  end


  private

  def retrieve_env
    ENV['ENVIRONMENT']
  end

  # @void
  def define_default customer, task
    @customer = customer.to_i
    now = Date.today
    yesterday = now.prev_day

    if task == 'return_updater'
      now = now.prev_day
    else
      now = now.prev_day 7
    end

    @s_period = DateTime.new(now.year, now.month, now.day, 0, 0, 0)
    @e_period = DateTime.new(yesterday.year, yesterday.month, yesterday.day, 23, 59, 59)

    nil
  end

  # @void
  def define_customer(customer = 0)
    @customer = customer.to_i
    nil
  end

  # @void
  def define_period_start period
    @s_period = Date.new(period.year, period.month, period.day)
    nil
  end

  # @void
  def define_period_end period
    @e_period = Date.new(period.year, period.month, period.day)
    nil
  end

  # @void
  def validate_args
    args = ARGV
    case args.count
    when 1
      define_default 0, args[0]
      true
    when 2
      define_customer 0
      define_period_start Date.parse(args[0])
      define_period_end Date.parse(args[1])
      true
    when 3
      define_customer args[0]
      define_period_start Date.parse(args[1])
      define_period_end Date.parse(args[2])
      true
    else
      puts "Parâmetros inválidos. Verifique o comando e tente novamente."
      exit 1
    end

    puts "Aguarde... \n"
    nil
  end
end