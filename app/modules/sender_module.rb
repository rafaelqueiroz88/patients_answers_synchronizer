#!/usr/bin/ruby

require 'pony'

# This class should receive subject, message and address and then send e-mails
class SenderModule

  def initialize
    define_configuration
  end

  def define_message message
    @message = message
  end

  def define_subject subject
    @subject = subject
  end

  def define_to_address address
    @to_address = address
  end

  def send_email
    Pony.mail(
      to: @to_address, via: :smtp, subject: @subject, body: @message, via_options:
      {
        address: 'smtp.gmail.com',
        port: '587',
        user_name: @setup[0],
        password: @setup[1],
        authentication: :plain,
        domain: 'hfocus.com.br'
      }
    )
  end


  private

  def define_configuration
    @setup = [ENV["MAIL_ADDRESS"], ENV["MAIL_PASS"]]
  end
end
