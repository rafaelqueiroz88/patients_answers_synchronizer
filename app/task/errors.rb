#!/usr/bin/ruby

# This class group the collection and then recover Mandrill status to a register
class Errors

  require_relative '../modules/cli_module'
  require_relative '../modules/mysql_module'
  require_relative '../modules/patient_module'
  require_relative '../modules/sender_module'

  # Constructor method
  def initialize env
    @cli = CLI.new
    @mysql = MysqlMaestro.new env
    @sender = SenderModule.new
  end

  def start_procedure

    customer = @cli.retrieve_customer
    p_start = @cli.retrieve_period_start
    p_end = @cli.retrieve_period_end
    collection = @mysql.retrieve_duplicated_errors customer, p_start, p_end

    subject = 'Processo de exclusão dos erros repetidos'
    message = 'O processo de exclusão de erros repetidos inicializado.'
    define_email_setup subject, message
    send_notification

    if collection.count.positive?
      interviews = Patient.new @mysql, customer

      interviews.define_collection collection
      interviews.remove_patients

      result = interviews.retrieve_results

      subject = 'Processo de exclusão de erros concluído com sucesso'
      message = 'Processo de exclusão de erros concluído com sucesso.'
      message = "#{message} #{result[0]} verificados.\n"
      message = "#{message}Excluídos: #{result[1]}/#{result[0]}.\n"
      message = "#{message}Falhas (salvos no log de erros): #{result[2]}/#{result[0]}."
    else
      subject = 'Falha durante o processo de exclusão de repetidas'
      message = "Processo de exclusão de respostas concluído\n"
      message = "#{message}Nenhum registro disponível para exclusão."
    end

    define_email_setup subject, message
    send_notification
    print message
    true
  end

  # Main task here
  # This task find patients whose Area and Quiz ID wasn't set and then set it
  def start_process

    customer = @cli.retrieve_customer
    p_start = @cli.retrieve_period_start
    p_end = @cli.retrieve_period_end
    collection = @mysql.retrieve_unset_patients customer, p_start, p_end

    subject = 'Atualizando o Retorno dos E-mails'
    message = 'O processo de atualização do Retorno_Email acabou de ser iniciado.'
    define_email_setup subject, message
    # send_notification

    if collection.count.positive?
      interviews = Patient.new @mysql, customer

      interviews.define_collection collection
      interviews.update_patients p_start, p_end

      result = interviews.retrieve_results

      subject = 'Processo de busca de ID e Área inicializado'
      message = 'Processo de busca de ID e Área finalizado.'
      message = "#{message} #{result[0]} verificados.\n"
      message = "#{message}Atualizados: #{result[1]}/#{result[0]}.\n"
      message = "#{message}Falhas (salvos no log de erros): #{result[2]}/#{result[0]}."
    else
      subject = 'Falha durante o processo de atualização'
      message = "Processo de atualização concluído\n"
      message = "#{message}Nenhum registro disponível para ser atualizado."
    end

    define_email_setup subject, message
    send_notification
    print message
    true
  end


  private

  def define_email_setup(subject, message, email = 'rafael.queiroz@hfocus.com.br')
    @sender.define_to_address email
    @sender.define_subject subject
    @sender.define_message message
  end

  def send_notification
    @sender.send_email
  end
end