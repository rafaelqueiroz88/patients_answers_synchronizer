# Ruby setup
ruby >= 2.6
# do NOT require rails

gem install excon
gem install mandrill-api
gem install mongo
gem install mysql2
gem install dotenv
gem install pony
gem install progress_bar

# Environment setup
$ sudo apt-get install mysql-server mysql-client libmysqlclient-dev

# Others
# Require date class which is native

Create a table with this:

create table log_mandrill_error
(
	id int auto_increment not null,
	message text null,
	customer_id int null,
	interviewed_id int null,
	email text null,
	process text null,
	created_at date null,
	constraint log_mandrill_error_pk
		primary key (id)
)
comment 'A table designed to log Mandrill Errors';

create table pacientes_disparos
(
	interviewee_email_logs_id varchar null,
	base_pacientes_id int null,
	atualizacoes int null,
	created_at DATETIME null
)
comment 'A table designed to link mongodb ids to mariadb ids';