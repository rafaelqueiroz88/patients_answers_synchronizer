FROM ruby:2.6

RUN gem install excon
RUN gem install mandrill-api
RUN gem install mongo
RUN gem install mysql2
RUN gem install dotenv
RUN gem install pony
RUN gem install progress_bar

WORKDIR /app

COPY app/ .

# Atualiza detalhes dos envios (tabela mandrill_email_status)
CMD [ "ruby", "/app/app.rb", "status_updater" ]

# Atualiza o status do envio (coluna Return_Email)
# CMD [ "ruby", "/app/app.rb", "return_updater" ]